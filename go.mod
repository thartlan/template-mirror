module gitlab.cern.ch/thartlan/template-mirror

go 1.15

require (
	github.com/gophercloud/gophercloud v0.13.0
	k8s.io/klog/v2 v2.3.0
)
