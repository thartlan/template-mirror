package main

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/containerinfra/v1/clustertemplates"

	"k8s.io/klog/v2"
)

func main() {
	opts, err := openstack.AuthOptionsFromEnv()
	if err != nil {
		klog.Fatalf("could not get auth options: %v", err)
	}
	klog.Info("Initialised auth options")

	provider, err := openstack.AuthenticatedClient(opts)
	if err != nil {
		klog.Fatalf("could not create provider client: %v", err)
	}

	sourceClient, err := openstack.NewContainerInfraV1(provider, gophercloud.EndpointOpts{
		Region: "cern",
	})
	if err != nil {
		klog.Fatalf("could not create source client: %v", err)
	}

	destinationClient, err := openstack.NewContainerInfraV1(provider, gophercloud.EndpointOpts{
		Region: "magnum_t",
	})
	if err != nil {
		klog.Fatalf("could not create destination client: %v", err)
	}

	sourcePages, err := clustertemplates.List(sourceClient, clustertemplates.ListOpts{}).AllPages()
	if err != nil {
		klog.Fatalf("could not list templates: %v", err)
	}

	sourceTemplates, err := clustertemplates.ExtractClusterTemplates(sourcePages)
	if err != nil {
		klog.Fatalf("could not extract templates: %v", err)
	}

	destinationPages, err := clustertemplates.List(destinationClient, clustertemplates.ListOpts{}).AllPages()
	if err != nil {
		klog.Fatalf("could not list templates in destination: %v", err)
	}

	destinationTemplates, err := clustertemplates.ExtractClusterTemplates(destinationPages)
	if err != nil {
		klog.Fatalf("could not extract templates in destination: %v", err)
	}

	for _, tmpl := range sourceTemplates {
		if tmpl.Public {
			var exists bool
			for _, t := range destinationTemplates {
				if tmpl.Name == t.Name {
					exists = true
				}
			}
			if exists {
				klog.Infof("Skipping existing template %s", tmpl.Name)
				continue
			}

			klog.Infof("Copying template %s", tmpl.Name)
			f := false
			var dockerVolumeSize *int
			if tmpl.DockerVolumeSize != 0 {
				*dockerVolumeSize = tmpl.DockerVolumeSize
			}
			newTemplateOpts := clustertemplates.CreateOpts{
				COE:                 tmpl.COE,
				DNSNameServer:       tmpl.DNSNameServer,
				DockerStorageDriver: tmpl.DockerStorageDriver,
				DockerVolumeSize:    dockerVolumeSize,
				ExternalNetworkID:   tmpl.ExternalNetworkID,
				FixedNetwork:        tmpl.FixedNetwork,
				FixedSubnet:         tmpl.FixedSubnet,
				FlavorID:            tmpl.FlavorID,
				FloatingIPEnabled:   &tmpl.FloatingIPEnabled,
				HTTPProxy:           tmpl.HTTPProxy,
				HTTPSProxy:          tmpl.HTTPSProxy,
				ImageID:             tmpl.ImageID,
				InsecureRegistry:    tmpl.InsecureRegistry,
				KeyPairID:           tmpl.KeyPairID,
				Labels:              tmpl.Labels,
				MasterFlavorID:      tmpl.MasterFlavorID,
				MasterLBEnabled:     &tmpl.MasterLBEnabled,
				Name:                tmpl.Name,
				NetworkDriver:       tmpl.NetworkDriver,
				NoProxy:             tmpl.NoProxy,
				Public:              &f,
				RegistryEnabled:     &tmpl.RegistryEnabled,
				ServerType:          tmpl.ServerType,
				TLSDisabled:         &tmpl.TLSDisabled,
				VolumeDriver:        tmpl.VolumeDriver,
			}

			createdTemplate, err := clustertemplates.Create(destinationClient, newTemplateOpts).Extract()
			if err != nil {
				klog.Fatalf("could not create cluster template %s: %v", tmpl.Name, err)
			}

			klog.Infof("Created template %s %s", createdTemplate.UUID, createdTemplate.Name)
		}
	}
}
